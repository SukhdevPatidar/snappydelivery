import {Component, Injectable} from '@angular/core';

import * as firebase from "firebase";
import {ApiProvider} from "../api/api";

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
@Injectable()
export class FirebaseProvider {

  config = {
    apiKey: "AIzaSyAz2MRG8cjNCuakdo2OyiCSZ1Gm4IMP6tk",
    authDomain: "snappydelivery-dd9cf.firebaseapp.com",
    databaseURL: "https://snappydelivery-dd9cf.firebaseio.com",
    projectId: "snappydelivery-dd9cf",
    storageBucket: "snappydelivery-dd9cf.appspot.com",
    messagingSenderId: "126030706169",
    appId: "1:126030706169:web:01b3c72ce23ac426ffb928",
    serverKey: "AAAAHVgD9fk:APA91bEbwcF_SppkObrptDZdr9X6A-IugMaL_o4n8193RAI3dEh75yEpQ8PO1Cj_XvPFRsxBkkTpS9IhJZn-51D4BotjFPtFJntSuAWGl1wkElmw6kwL__ov169whOojGO0iVNzalSYp"
  };

  constructor(
    public apiService: ApiProvider
  ) {
    console.log('Hello FirebaseProvider Provider');
  }

  initializeFirebase() {
    firebase.initializeApp(this.config);
  }

  private getConversationsRef = (userId) => {
    return firebase.database().ref('accounts/' + userId + "/conversations/");
  }

  private getConversationRef = (firstUser, secondUser, key = null) => {
    var keyStr = "";
    if (key) {
      keyStr = "/" + key;
    }
    return firebase.database().ref('accounts/' + firstUser + "/conversations/" + secondUser + keyStr);
  }

  public getConversationId(userId, recieverId) {
    return this.getConversationRef(userId, recieverId);
  }

  public updateUnreadCount = (firstUser, secondUser, lastMessage = null) => {
    this.getConversationRef(secondUser, firstUser, "unread_msg").transaction(function (unread_msg) {
      if (!unread_msg) {
        unread_msg = 0;
      }
      return unread_msg + 1;
    });

    this.getConversationRef(secondUser, firstUser, "updated_at").transaction(function (updated_at) {
      return firebase.database.ServerValue.TIMESTAMP;
    });

    this.getConversationRef(firstUser, secondUser, "updated_at").transaction(function (updated_at) {
      return firebase.database.ServerValue.TIMESTAMP;
    });

    if (lastMessage) {
      this.getConversationRef(secondUser, firstUser, "lastMessage").transaction(function (updated_at) {
        return lastMessage;
      });

      this.getConversationRef(firstUser, secondUser, "lastMessage").transaction(function (updated_at) {
        return lastMessage;
      });
    }
  }

  public clearUnreadMsg = (firstUser, secondUser) => {
    this.getConversationRef(firstUser, secondUser, "unread_msg").transaction(function (unread_msg) {
      return 0;
    });
  }

  // getConversationRef() {
  // 	return firebase.database().ref('conversations/'+userId+"/conversations/"+recieverId);
  // }

  public inserConversationId(senderId, recieverId, conversationId) {
    this.getConversationRef(senderId, recieverId).set({
      convresationId: conversationId
    });
    this.getConversationRef(recieverId, senderId).set({
      convresationId: conversationId
    });
  }

  public creatConversation(senderId, recieverId, message) {
    let conversationId = firebase.database().ref('conversations').push({
      users: [senderId, recieverId]
    }).key;
    this.addMsgToConversation(message, conversationId);

    return conversationId;
  }

  public getConversations(userId) {
    return this.getConversationsRef(userId);
  }

  public addMsgToConversation(msg, conversationId) {
    return firebase.database().ref('conversations/' + conversationId + "/messages").push(msg);
  }

  public getMessages(conversationId) {
    return firebase.database().ref('conversations/' + conversationId + "/messages");
  }


  private configureNotificaitons(registrationIds, title, message, data) {
    let notificationData = {
      "registration_ids": registrationIds,
      "notification": {
        "title": title,
        "body": message,
        "sound": 1,
        "badge": 1,
        'color': '#419bf9',
        'icon': 'notification_icon',
      },
      "data": {
        "data": data
      }
    };

    var headers = {
      'Content-Type': "application/json",
      'Authorization': "key=" + this.config.serverKey
    }

    let url = "https://fcm.googleapis.com/fcm/send";

    return this.apiService.postRequest(url, notificationData, headers);
  }

  public sendMessageNotifcations(pushToken, senderName, userId, message, conversationId) {
    this.configureNotificaitons([pushToken], senderName, message,
      {
        type: "message_recieved",
        sender_id: userId,
        conversationTye: 'user_conversation',
        conversationId: conversationId
      });
  }

}
