import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the StorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class StorageProvider {

  	public itemsCount = 0;


  	constructor(public http: HttpClient) {
  		console.log('Hello StorageProvider Provider');
  	}


  	 // User

  	 setUser(user) {
  	 	window.localStorage.setItem('user', JSON.stringify(user));
  	 }

  	 getUser() {
  	 	let user = window.localStorage.getItem('user');
  	 	return (!user)?{}:JSON.parse(user);
  	 }

  	 removeUser() {
  	 	window.localStorage.removeItem('user');
  	 }
  	 
  	 
    // Login Status

    setLoggedIn() {
    	window.localStorage.setItem('isLoggedIn', "true");
    }

    isLoggedIn() {
    	return window.localStorage.getItem('isLoggedIn') == 'true' ? true : false;
    }

    removeLoggedIn() {
    	window.localStorage.removeItem('isLoggedIn');
    }

    // Login Details

    setLoginDetails(loginDetails) {
    	window.localStorage.setItem('loginDetails', JSON.stringify(loginDetails));
    }

    getLoginDetails() {
    	let loginDetails = window.localStorage.getItem('loginDetails');
    	return (!loginDetails)?{}:JSON.parse(loginDetails);
    }

    removeLoginDetails() {
    	window.localStorage.removeItem('loginDetails');
    }

    
    // Localizations
    setLanguage(language) {
    	window.localStorage.setItem('default_language', language);
    }

    getLanguage() {
    	let language = window.localStorage.getItem('default_language');
    	return language ? language : "es";
    }

}
