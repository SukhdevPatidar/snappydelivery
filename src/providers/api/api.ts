import {Injectable} from '@angular/core';
import {Http, RequestOptions, Headers} from '@angular/http';
import {Platform, LoadingController, AlertController, ToastController, NavController} from 'ionic-angular';
import {Network} from '@ionic-native/network';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/map';


declare var navigator: any;
declare var Connection: any;

/*
  Generated class for the ApiService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
  	*/
@Injectable()
export class ApiProvider {
  loading;
  isNetworkDisconnected = false;
  requestTimeout = 30; //seconds
  isLoadingShow = false;
  public isDevice = false;

  public rootNavController: NavController;

  constructor(private network: Network, public http: Http, private platform: Platform, public loadingCtrl: LoadingController, private alertCtrl: AlertController, private toastCtrl: ToastController) {
    var ref = this;

    this.network.onDisconnect().subscribe(() => {
      ref.hideLoading();

      if (!this.isNetworkDisconnected) {
        ref.isNetworkDisconnected = true;
        ref.showToast("network disconnected");
        console.log('network disconnected!');
      }
    });

    // let connectSubscription =
    this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      ref.isNetworkDisconnected = false;
    });

    // this.checkNetwork();
  }


  showLoading() {

    this.isLoadingShow = true;
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  hideLoading() {
    if (this.loading) {
      this.isLoadingShow = false;
      this.loading.dismiss();
      this.loading = null;
    }
  }

  showAlert(msg, callBack?: Function) {
    let alert = this.alertCtrl.create({
      title: 'ALERT!',
      subTitle: msg,
      buttons: [{
        text: 'Ok',
        role: 'ok',
        handler: () => {
          if (callBack) {
            callBack();
          }
        }
      }]
    });
    alert.present();
  }

  showToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();

  }


  getBaseApiUrl() {
    if (this.isDevice) {
      // return "http://localhost:8888/fernado_Cabrera_Mexico/SnappyDelivery/adminpanel/";
      return "https://snappypanel.com/adminpanel/";
    }
    return "http://localhost:8100/apiUrl/";
  }

  getApiUrl() {
    return this.getBaseApiUrl();
  }

  apiMethods() {
    return {
      login: "login",
      signup: "signup",
      resetPassword: "resetPassword",
      deliveryOrders: "deliveryOrders",
      orderStatusUpdate: "orderStatusUpdate",
      updateToken: "updateToken",
      deliveryBoyStatusChange: "deliveryBoyStatusChange",
      updateUserCurrentLocation: "updateUserCurrentLocation",
      deliveryBoyStatus: "deliveryBoyStatus",
      logoutUser: "logoutUser",
      appVersion: "appVersion",
    };
  }

  postRequest(url, postData, header = {}) {
    var ref = this;

    return new Promise((resolve, reject) => {
      postData = postData ? postData : {};

      let headers = new Headers(Object.assign({'Content-Type': 'application/json'}, header));
      let options = new RequestOptions({headers: headers});

      this.http.post(url, postData, options)
        .timeout(20000)
        .map((res) => res.json())
        .subscribe(data => {
            console.log("Response " + JSON.stringify(data));
            resolve(data);
          },
          (error) => {
            console.log("Error " + JSON.stringify(error));
            ref.hideLoading();
            reject(error);
          }
        );
    });
  }

  getRequest(url) {
    var ref = this;

    return new Promise((resolve, reject) => {

      this.http.get(url)
        .timeout(20000)
        .map(res => res.json())
        .subscribe(data => {
            console.log("Api Url response : " + JSON.stringify(data));
            resolve(data);
          },
          error => {
            ref.hideLoading();
            console.log("Error " + JSON.stringify(error));
            reject(error);
          });
    });
  }


  QueryStringBuilder(params) {
    var segments = [], value;
    for (var key in params) {
      value = params[key];
      segments.push(encodeURIComponent(key) + "=" + encodeURIComponent(value));
    }
    return segments.join("&");
  }


  checkUserLogin(email, password) {
    var params = {
      'email': email,
      'password': password,
      'role': 'delivery_boy'
    };
    let url = this.getApiUrl() + this.apiMethods().login;
    return this.postRequest(url, params);
  }

  resetPassword(email) {
    var params = {
      'email': email
    };
    let url = this.getApiUrl() + this.apiMethods().resetPassword;
    return this.postRequest(url, params);
  }

  deliveryBoyStatusChange(userId, status) {
    var params = {
      'user_id': userId,
      'status': status
    };
    let url = this.getApiUrl() + this.apiMethods().deliveryBoyStatusChange;
    return this.postRequest(url, params);
  }

  deliveryBoyLocationUpdate(userId, latitude, longitude) {
    var params = {
      'user_id': userId,
      'latitude': latitude,
      'longitude': longitude
    };
    let url = this.getApiUrl() + this.apiMethods().updateUserCurrentLocation;
    return this.postRequest(url, params);
  }

  deliveryBoyStatus(userId) {
    var params = {
      'user_id': userId
    };
    let url = this.getApiUrl() + this.apiMethods().deliveryBoyStatus;
    return this.postRequest(url, params);
  }

  logoutUser(userId) {
    var params = {
      'user_id': userId
    };
    let url = this.getApiUrl() + this.apiMethods().logoutUser + "?" + this.QueryStringBuilder(params);
    return this.getRequest(url);
  }

  deliveryOrders(userId) {
    var params = {
      'delivery_userid': userId
    };
    let url = this.getApiUrl() + this.apiMethods().deliveryOrders + "?" + this.QueryStringBuilder(params);
    return this.getRequest(url);
  }

  orderStatusUpdate(order_id, delivery_user, order_status) {
    var params = {
      'order_id': order_id,
      'delivery_user': delivery_user,
      'order_status': order_status
    };
    let url = this.getApiUrl() + this.apiMethods().orderStatusUpdate;
    return this.postRequest(url, params);
  }

  updateToken(user_id, push_token) {
    var params = {
      'user_id': user_id,
      'push_token': push_token,
    };
    let url = this.getApiUrl() + this.apiMethods().updateToken;
    return this.postRequest(url, params);
  }

  appVersion(platform, version) {
    let url = this.getApiUrl() + this.apiMethods().appVersion + "?app=snappy_go&platform=" + platform + "&app_version=" + version;
    return this.getRequest(url);
  }

}
