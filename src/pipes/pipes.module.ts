import { NgModule } from '@angular/core';
import { ValuesPipe } from './values/values';
@NgModule({
	declarations: [ValuesPipe],
	imports: [],
	exports: [ValuesPipe]
})
export class PipesModule {}
