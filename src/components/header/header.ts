import { Component , EventEmitter } from '@angular/core';
import { NavController } from 'ionic-angular';

import {HomePage} from '../../pages/home/home';

import { ApiProvider } from '../../providers/api/api';
import { StorageProvider } from '../../providers/storage/storage';

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
 @Component({
 	selector: 'header',
 	templateUrl: 'header.html',
 	inputs: [ 'hideBackButton', 'showLogout', 'showRefreshBtn'],
 	outputs: ['onRefresh']
 })
 export class HeaderComponent {
 	public hideBackButton = false;
 	public showRefreshBtn = false;
 	
 	public showLogout = false;
 	onRefresh: EventEmitter<number>;

 	text: string;

 	constructor(public navCtrl: NavController,  public apiProvider: ApiProvider,  public storage: StorageProvider) {
 		console.log('Hello HeaderComponent Component');
 		this.text = 'Hello World';
 		this.onRefresh = new EventEmitter<number>();
 	}

 	onLogout() {
 		this.apiProvider.showLoading();
 		this.apiProvider.logoutUser(this.storage.getUser().id).then(data => {
 			this.apiProvider.hideLoading();
 			this.logoutSuccess(); 			
 		},
 		(error) => {
 			this.apiProvider.hideLoading();
 			this.logoutSuccess();
 			console.log("Login Error " + JSON.stringify(error));
 		});

 		
 	}

 	logoutSuccess = () => {
 		this.storage.removeUser();
 		this.storage.removeLoggedIn();
 		this.storage.removeLoginDetails();
 		this.apiProvider.rootNavController.setRoot(HomePage);
 	}

 	onRightBtnClick() {
 		this.onRefresh.emit();
 	}
 }
