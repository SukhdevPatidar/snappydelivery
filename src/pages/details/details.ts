import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, Events} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {StorageProvider} from '../../providers/storage/storage';

import {LocationtrackPage} from '../locationtrack/locationtrack';
import {HomePage} from "../home/home";


/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  groupedOrders = [];
  groupedOrdersOriginal = [];
  user;
  searchInput = "";

  showOrderRefreshSpinner = true;
  userStatusUpdating = false;
  userStatus = false;

  constructor(public navCtrl: NavController, public apiProvider: ApiProvider, public zone: NgZone, public events: Events, public storage: StorageProvider) {
    this.user = this.storage.getUser();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');

    this.getOrders();
    this.getUserStatus();
    this.events.subscribe("REFRESH_ORDER", () => {
      this.getOrders();
    });
  }

  getDateObj(dateStr: string) {
    dateStr = dateStr.replace(" ", "T");
    return new Date(dateStr);
  }

  getOrders() {
    this.showOrderRefreshSpinner = true;
    this.apiProvider.deliveryOrders(this.storage.getUser().id).then(data => {

        this.zone.run(() => {
          this.showOrderRefreshSpinner = false;

          if (data['status']) {
            let orders = data['data'];
            this.groupOrdersByDate(orders);
          } else if (!data['status']) {
            let msg = data['error'];
            this.apiProvider.showAlert(msg);
            this.groupedOrders = [];
            this.groupedOrdersOriginal = [];
          }
        });

      },
      (error) => {
        this.zone.run(() => {
          this.showOrderRefreshSpinner = false;
        });
        console.log("Login Error " + JSON.stringify(error));
      });
  }

  userStatusChanged() {
    this.userStatusUpdating = true;
    this.apiProvider.deliveryBoyStatusChange(this.storage.getUser().id, this.userStatus).then(data => {

        this.zone.run(() => {
          this.userStatusUpdating = false;
          this.getUserStatus();
        });

      },
      (error) => {
        this.zone.run(() => {
          this.userStatusUpdating = false;
        });
        console.log("Login Error " + JSON.stringify(error));
      });
  }


  getUserStatus = () => {
    this.apiProvider.deliveryBoyStatus(this.storage.getUser().id).then(res => {
        this.zone.run(() => {
          if (res['status']) {
            let data = res['data'];
            this.userStatus = (parseInt(data['status']) == 1) ? true : false;
          } else {
            this.userStatus = false;
          }
        });

      },
      (error) => {
        console.log("Login Error " + JSON.stringify(error));
      });
  }

  onLogout() {
    this.apiProvider.showLoading();
    this.apiProvider.logoutUser(this.storage.getUser().id).then(data => {
        this.apiProvider.hideLoading();
        this.logoutSuccess();
      },
      (error) => {
        this.apiProvider.hideLoading();
        this.logoutSuccess();
        console.log("Login Error " + JSON.stringify(error));
      });
  }

  logoutSuccess = () => {
    this.storage.removeUser();
    this.storage.removeLoggedIn();
    this.storage.removeLoginDetails();
    this.apiProvider.rootNavController.setRoot(HomePage);
    this.events.publish("LOGGED_IN", false);
  }


  onOrderRefresh = () => {
    this.getOrders();
  }

  groupOrdersByDate(orders) {

    var groupedByDate = {};

    for (var i = 0; i < orders.length; ++i) {
      let order = orders[i];
      let group = groupedByDate[order['created_at']];
      if (!group) {
        group = [];
      }
      group.push(order);
      groupedByDate[order['created_at']] = group;
    }

    var grouped = [];
    Object.keys(groupedByDate).map((value, index) => {
      grouped.push({
        date: value,
        orders: groupedByDate[value],
        show: false
      });
    });

    this.groupedOrders = grouped;
    this.groupedOrdersOriginal = grouped;
  }

  getProductSubTotal(product) {
    var price = 0;
    if (product.promotional_price) {
      price = parseFloat(product.promotional_price);
    } else {
      price = parseFloat(product.price);
    }
    return price * parseInt(product.quantity);
  }

  //getSubtotal
  getOrderSubTotal(products) {
    var total = 0;
    products.forEach((value, key) => {
      var price = 0;
      if (value.promotional_price) {
        price = parseFloat(value.promotional_price);
      } else {
        price = parseFloat(value.price);
      }
      total = total + this.calculateSuppliesPrice(value.supplies) * parseInt(value.quantity);
      total = total + price * parseInt(value.quantity);
    });
    return total;
  }

  calculateSuppliesPrice(suppliesStr) {
    if (suppliesStr) {
      let supplies = JSON.parse(suppliesStr);
      var totalSuppliesPrice = 0;
      for (var i = 0; i < supplies.length; i++) {
        let supply = supplies[i];
        totalSuppliesPrice = totalSuppliesPrice + parseFloat(supply.supplies_price);
      }
      return totalSuppliesPrice;
    }
    return 0;
  }

  getOrderTotal(order) {
    var total = this.getOrderSubTotal(order.products) + parseFloat(order.delievery_price);
    return total;
  }

  goToTrackingPage(order) {
    this.navCtrl.push(LocationtrackPage, {order: order});
  }

  searchOrders(event) {
    let searchText = this.searchInput;
    var filtered = [];

    if (searchText && searchText.length > 0) {
      this.groupedOrdersOriginal.forEach((value, key) => {
        var filteredOrders = [];
        value.orders.forEach((value, key) => {
          if (value.shipping_address.name.indexOf(searchText) !== -1 || value.shipping_address.mobile.indexOf(searchText) !== -1 || value.id.indexOf(searchText) !== -1) {
            filteredOrders.push(value);
          }
        });

        if (filteredOrders && filteredOrders.length > 0) {
          filtered.push({
            date: value.date,
            orders: filteredOrders,
            show: true
          })
        }

      });
      this.groupedOrders = filtered;
    } else {
      this.groupedOrders = this.groupedOrdersOriginal;
    }
  }


  orderComplete(order) {
    this.apiProvider.showLoading();
    this.apiProvider.orderStatusUpdate(order.id, this.storage.getUser().id, 'completed').then(data => {
        this.apiProvider.hideLoading();

        if (data['status']) {
          this.apiProvider.showAlert("Pedido entregado", () => {
            this.getOrders();
          });
        } else if (!data['status']) {
          let msg = data['error'];
          this.apiProvider.showAlert(msg);
        }
      },
      (error) => {
        this.apiProvider.hideLoading();
        console.log("Login Error " + JSON.stringify(error));
      });
  }

  goToChatPage(order) {
    this.navCtrl.push('page-chat-detail', {
      'receiver': JSON.stringify(order.user),
      'order_id': order.id
    });
  }
}
