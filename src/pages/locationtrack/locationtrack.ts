import {Component, ViewChild, ElementRef, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {ApiProvider} from '../../providers/api/api';
import {Geolocation} from '@ionic-native/geolocation';

/**
 * Generated class for the LocationtrackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;

const demoLat = -23.3343481;
const demoLng = -60.5637841;

@IonicPage()
@Component({
    selector: 'page-locationtrack',
    templateUrl: 'locationtrack.html',
})
export class LocationtrackPage {
    @ViewChild('map') mapElement: ElementRef;
    map: any;

    currentPosMarker: any;
    destinationMarker: any;

    count = 0;
    userPathPolyline: any;
    positionWatchSubscription: any;
    originLocation: any;
    destinationLocation: any;

    order: any;

    constructor(public platform: Platform, public apiProvider: ApiProvider, public geolocation: Geolocation, public zone: NgZone, public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad LocationtrackPage');
        this.order = this.navParams.get("order");
        let shippingAdd = this.order.shipping_address;
        this.destinationLocation = {
            lat: shippingAdd.latitude,
            lng: shippingAdd.longitude,
        }
        this.platform.ready().then(() => {
            this.loadMap();
            this.getCurrentLocation();
        });
    }

    ionViewDidUnload() {
        if (this.positionWatchSubscription) {
            this.positionWatchSubscription.unsubscribe();
        }
    }

    getCurrentLocation = () => {

        if (this.apiProvider.isDevice) {
            this.geolocation.getCurrentPosition().then((resp) => {
                let lat = resp.coords.latitude;
                let lng = resp.coords.longitude;
                this.getRoutes({lat: lat, lng: lng});

            }).catch((error) => {
                console.log('Error getting location', error);
            });

            let watch = this.geolocation.watchPosition();

            this.positionWatchSubscription = watch.subscribe((resp) => {
                let lat = resp.coords.latitude;
                let lng = resp.coords.longitude;
                this.getRoutes({lat: lat, lng: lng});
            });
        } else {
            this.getRoutes({lat: demoLat, lng: demoLng});
        }
    }


    getRoutes = (originCoords) => {
        let currnetLoc = new google.maps.LatLng(originCoords.lat, originCoords.lng);
        if (this.map) {
            this.map.setCenter(currnetLoc);
            this.currentPosMarker.setPosition(currnetLoc);
        }


        // if(this.userPathPolyline) {
        // 	this.userPathPolyline.setMap(null);
        // 	this.userPathPolyline = null;
        // }

        if (!this.originLocation) {
            this.originLocation = originCoords;
            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer({
                map: this.map,
                preserveViewport: true
            });

            directionsService.route({
                origin: new google.maps.LatLng(this.originLocation.lat, this.originLocation.lng),
                destination: new google.maps.LatLng(this.destinationLocation.lat, this.destinationLocation.lng),
                waypoints: [{
                    stopover: false,
                    location: new google.maps.LatLng(originCoords.lat, originCoords.lng)
                }],
                travelMode: google.maps.TravelMode.DRIVING
            }, (response, status) => {

                if (status === google.maps.DirectionsStatus.OK) {
                    this.userPathPolyline = new google.maps.Polyline({
                        path: [],
                        strokeColor: '#A82128',
                        strokeWeight: 3
                    });
                    var bounds = new google.maps.LatLngBounds();


                    var legs = response.routes[0].legs;
                    for (var i = 0; i < legs.length; i++) {
                        var steps = legs[i].steps;
                        for (var j = 0; j < steps.length; j++) {
                            var nextSegment = steps[j].path;
                            for (var k = 0; k < nextSegment.length; k++) {
                                this.userPathPolyline.getPath().push(nextSegment[k]);
                                bounds.extend(nextSegment[k]);
                            }
                        }
                    }
                    this.map.fitBounds(bounds);
                    this.userPathPolyline.setMap(this.map);
                } else {
                    // window.alert('Directions request failed due to ' + status);
                }
            });
        }

    }

    loadMap() {
        // this.geocoder = new google.maps.Geocoder;
        var latLng = new google.maps.LatLng(demoLat, demoLng);
        var geocoder = new google.maps.Geocoder;


        let mapOptions = {
            center: latLng,
            zoom: 15,
            panControl: true,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            rotateControl: false,
            fullscreenControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        google.maps.event.trigger(this.map, 'resize')

        var lat = latLng.lat();
        var lng = latLng.lng();

        if (!this.destinationMarker) {
            this.addDestinationMarker(this.destinationLocation.lat, this.destinationLocation.lng);
        }
        if (!this.currentPosMarker) {
            this.addCurrentPosMarker(lat, lng);
        }

    }


    addCurrentPosMarker = (lat, lng) => {
        let marker = new google.maps.Marker({
            map: this.map,
            draggable: false,
            icon: {
                path: google.maps.SymbolPath.CIRCLE,
                fillColor: "blue",
                fillOpacity: 1.0,
                strokeColor: "#fff",
                strokeOpacity: 1.0,
                strokeWeight: 0,
                scale: 10
            },
            position: new google.maps.LatLng(lat, lng),
        });

        google.maps.event.addListener(marker, 'dragend', (marker) => {
            var latLng = marker.latLng;
        });
        this.currentPosMarker = marker;
    }

    addDestinationMarker = (lat, lng) => {
        let marker = new google.maps.Marker({
            map: this.map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(lat, lng),
        });

        google.maps.event.addListener(marker, 'dragend', (marker) => {
            var latLng = marker.latLng;
        });
        this.destinationMarker = marker;
    }

}
