import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocationtrackPage } from './locationtrack';

@NgModule({
  declarations: [
    LocationtrackPage,
  ],
  imports: [
    IonicPageModule.forChild(LocationtrackPage),
  ],
})
export class LocationtrackPageModule {}
