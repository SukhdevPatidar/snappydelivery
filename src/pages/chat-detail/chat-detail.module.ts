import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatDetailPage } from './chat-detail';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ChatDetailPage
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ChatDetailPage)
  ],
  exports: [
    ChatDetailPage
  ]
})

export class ChatDetailPageModule { }
