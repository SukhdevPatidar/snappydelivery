import {Component, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, Content} from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";
import {FirebaseProvider} from "../../providers/firebase/firebase";
import moment from 'moment';
import 'moment/locale/es';
import {StorageProvider} from "../../providers/storage/storage";

@IonicPage({
  name: 'page-chat-detail',
  segment: 'chat'
})

@Component({
  selector: 'page-chat-detail',
  templateUrl: 'chat-detail.html'
})
export class ChatDetailPage {
  @ViewChild(Content) content: Content;

  receiver: any;
  message;
  order_id;
  convresationId;

  messageListner;
  messages;
  user;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public firebaseProvider: FirebaseProvider,
    public storage: StorageProvider
  ) {
    this.receiver = JSON.parse(this.navParams.get('receiver'));
    this.order_id = this.navParams.get('order_id');
    this.user = this.storage.getUser();
  }

  ionViewDidLoad() {
    this.startChat();
  }

  ionViewWillUnload() {
    console.log('ionViewWillUnload InboxPage');
    if (this.messageListner) {
      this.messageListner.off()
    }
  }

  startChat = () => {
    let userId = this.storage.getUser().id;
    let recieverId = this.receiver.id;

    this.firebaseProvider.getConversationId(userId, recieverId).once("value", (val) => {
      console.log(val);
      if (val.val()) {
        this.convresationId = val.val().convresationId;
        this.getMessages();
      }
    }, (error) => {
      console.error(error);
    });
  }

  getMessages() {
    let ref = this;
    this.messageListner = this.firebaseProvider.getMessages(this.convresationId);
    this.messageListner.on("value", (val) => {
      if (val.val()) {
        let messages = val.val();
        setTimeout(() => {
          this.firebaseProvider.clearUnreadMsg(this.storage.getUser().id, this.receiver.id);
        }, 1000);
        ref.messages = messages;
        ref.scrollToBottom();
      }
    })
  }

  scrollToBottom = () => {
    if (this.content && this.content.scrollToBottom) {
      this.content.scrollToBottom();
      setTimeout(() => {
        this.content.scrollToBottom();
      }, 300);
      setTimeout(() => {
        this.content.scrollToBottom();
      }, 1000);
    }
  }

  sendMessage = () => {
    if (this.message) {
      let userId = this.storage.getUser().id;
      let senderName = this.storage.getUser().name + " " + this.storage.getUser().l_name;
      let recieverId = this.receiver.id;
      let txtMsg = this.message;

      let message = {
        msg: txtMsg,
        type: "client_broker_message",
        time: (new Date()).toString(),
        sender: userId
      };
      if (this.order_id) {
        message['order_id'] = this.order_id;
      }

      if (!this.convresationId) {
        this.convresationId = this.firebaseProvider.creatConversation(userId, recieverId, message);
        this.firebaseProvider.inserConversationId(userId, recieverId, this.convresationId);
        this.getMessages();
      } else {
        this.firebaseProvider.addMsgToConversation(message, this.convresationId);
      }

      // send push notification
      if (this.receiver.push_token) {
        this.firebaseProvider.sendMessageNotifcations(this.receiver.push_token, senderName, this.storage.getUser().id, txtMsg, this.convresationId);
      }
      this.firebaseProvider.updateUnreadCount(userId, recieverId, message);

      this.message = "";
      this.scrollToBottom();
    }
  }

  isSenderMe(message) {
    if (parseInt(message.sender) == parseInt(this.storage.getUser().id)) {
      return true;
    }
    return false;
  }


  getFormatedTime(str) {
    var A_WEEK_OLD = moment().clone().subtract(7, 'days').startOf('day');
    if (moment(str).isAfter(A_WEEK_OLD)) {
      return moment(str).locale("es").calendar();
    } else {
      return moment(str).locale("es").format('DD/MM/YYYY hh:mm a');
    }
  }

  getImagePath(path) {
    if (path) {
      return this.api.getBaseApiUrl() + path;
    } else {
      return "./assets/imgs/placeholder.png"
    }
  }

}
