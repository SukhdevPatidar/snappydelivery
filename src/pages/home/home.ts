import {Component} from '@angular/core';
import {NavController, Events, Platform} from 'ionic-angular';

import {DetailsPage} from '../details/details';
import {ForgotpasswordPage} from '../forgotpassword/forgotpassword';

import {ApiProvider} from '../../providers/api/api';
import {StorageProvider} from '../../providers/storage/storage';

import {Firebase} from '@ionic-native/firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  DetailsPage = DetailsPage;
  ForgotPassword = ForgotpasswordPage;

  emailLogin;
  passwordLogin;

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public events: Events,
    public apiProvider: ApiProvider,
    public storage: StorageProvider,
    private firebase: Firebase
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
    this.apiProvider.rootNavController = this.navCtrl;

    if (this.storage.isLoggedIn()) {
      this.goToHomePage();
    }

    this.platform.ready().then(() => {
      this.setupFirebase();
    });

  }


  setupFirebase = () => {
    if (!this.apiProvider.isDevice) {
      return;
    }

    if (this.platform.is('ios')) {
      this.firebase.hasPermission().then(({isEnabled}) => {
        if (!isEnabled) {
          this.firebase.grantPermission();
        }
      });
    }

    this.firebase.setBadgeNumber(0);

    this.firebase.getToken()
      .then(token => {
        console.log(`The token is ${token}`);
        this.updateFirebaseToken(token);
      }) // save the token server-side and use it to push notifications to this device
      .catch(error => {
        console.error('Error getting token', error)
      });

    this.firebase.onTokenRefresh()
      .subscribe((token: string) => {
        this.updateFirebaseToken(token);
        console.log(`Got a new token ${token}`)
      });

    this.firebase.onNotificationOpen().subscribe((notification) => {
      if (!notification.tap) {
        this.apiProvider.showAlert(notification.title + " " + notification.body, () => {
          if (notification.action == 'order_assigned') {
            this.events.publish("REFRESH_ORDER");
          }
        });
      } else {
        this.apiProvider.showAlert(notification.title + " " + notification.body, () => {
          if (notification.action == 'order_assigned') {
            this.events.publish("REFRESH_ORDER");
          }
        });
      }
      this.events.publish("REFRESH_ORDER");
    });
  }

  updateFirebaseToken(token) {
    if (token) {
      localStorage.setItem('firebase_token', token);
      if (this.storage.getUser()) {
        this.apiProvider.updateToken(this.storage.getUser().id, token).then((data) => {
        });
      }
    }
  }

  login() {
    if (this.emailLogin.length > 0 && this.passwordLogin.length > 0) {
      this.apiProvider.showLoading();
      this.apiProvider.checkUserLogin(this.emailLogin, this.passwordLogin).then(data => {
          this.apiProvider.hideLoading();
          console.log("Login Response " + JSON.stringify(data));

          if (data['status']) {
            let user = data['data'];
            if (user) {
              this.storage.setLoginDetails({email: this.emailLogin, password: this.passwordLogin});
              this.storage.setUser(user);
              this.storage.setLoggedIn();
              this.goToHomePage();
            }
            this.updateFirebaseToken(localStorage.getItem("firebase_token"));
            this.events.publish("LOGGED_IN", true);

          } else if (!data['status']) {
            let msg = data['error'];
            this.apiProvider.showAlert(msg);
          }
        },
        (error) => {
          this.apiProvider.hideLoading();
          console.log("Login Error " + JSON.stringify(error));
        });
    } else if (this.emailLogin.length == 0) {
      this.apiProvider.showAlert('POR FAVOR, ENTRE CORREO');
    } else if (this.passwordLogin.length == 0) {
      this.apiProvider.showAlert('POR FAVOR, INGRESE CONTRASEÑA\n');
    }
  }

  goToHomePage() {
    this.navCtrl.push(DetailsPage);
  }


}
