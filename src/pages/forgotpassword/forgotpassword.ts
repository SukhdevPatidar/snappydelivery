import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-forgotpassword',
 	templateUrl: 'forgotpassword.html',
 })
 export class ForgotpasswordPage {
 	email;

 	constructor(public navCtrl: NavController, public navParams: NavParams, public apiProvider: ApiProvider) {
 	}

 	ionViewDidLoad() {
 		console.log('ionViewDidLoad ForgotpasswordPage');
 	}

 	forgotPassword() {
 		if (!this.email || this.email.length == 0) {
 			this.apiProvider.showAlert('PLEASE ENTER EMAIL');
 		} else {
 			this.apiProvider.showLoading();
 			this.apiProvider.resetPassword(this.email).then(data => {
 				this.apiProvider.hideLoading();
 				if(data['status']) {
 					this.apiProvider.showAlert(data['message']);
 					this.navCtrl.pop();
 				} else if(!data['status']) {
 					this.apiProvider.showAlert(data['message']);
 				}
 			},
 			(error) => {
 				this.apiProvider.hideLoading();
 				console.log("Login Error " +JSON.stringify(error));
 			});
 		}
 	}
 }
