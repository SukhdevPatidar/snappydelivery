import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {Http} from '@angular/http';

import {MyApp} from './app.component';

import {ComponentsModule} from './../components/components.module';
import {PipesModule} from '../pipes/pipes.module';

import {HomePage} from '../pages/home/home';
import {DetailsPageModule} from '../pages/details/details.module';
import {ForgotpasswordPageModule} from '../pages/forgotpassword/forgotpassword.module';
import {LocationtrackPageModule} from '../pages/locationtrack/locationtrack.module';

import {Network} from '@ionic-native/network';
import {HttpModule} from '@angular/http';
import {StorageProvider} from '../providers/storage/storage';
import {ApiProvider} from '../providers/api/api';
import {HttpClientModule} from '@angular/common/http';

import {Firebase} from '@ionic-native/firebase';
import {Geolocation} from '@ionic-native/geolocation';
import {BackgroundGeolocation} from '@ionic-native/background-geolocation';
import {FirebaseProvider} from '../providers/firebase/firebase';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AppVersion } from '@ionic-native/app-version';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ComponentsModule,
    HttpClientModule,
    DetailsPageModule,
    ForgotpasswordPageModule,
    LocationtrackPageModule,
    IonicModule.forRoot(MyApp),
    PipesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    BackgroundGeolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    Firebase,
    Network,
    StorageProvider,
    FirebaseProvider,
    Diagnostic,
    AppVersion
  ]
})
export class AppModule {
}
