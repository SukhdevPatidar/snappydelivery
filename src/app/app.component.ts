import {Component} from '@angular/core';
import {Platform, Events, AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {ApiProvider} from '../providers/api/api';
import {
  BackgroundGeolocation,
  BackgroundGeolocationConfig, BackgroundGeolocationEvents,
  BackgroundGeolocationResponse
} from '@ionic-native/background-geolocation';

import {HomePage} from '../pages/home/home';
import {StorageProvider} from "../providers/storage/storage";
import {FirebaseProvider} from "../providers/firebase/firebase";
import {Firebase} from "@ionic-native/firebase";
import {Diagnostic} from '@ionic-native/diagnostic';
import {AppVersion} from '@ionic-native/app-version';
import {Geolocation} from "@ionic-native/geolocation";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public apiProvider: ApiProvider,
    private backgroundGeolocation: BackgroundGeolocation,
    public storage: StorageProvider,
    public firebaseProvider: FirebaseProvider,
    private firebase: Firebase,
    private events: Events,
    private diagnostic: Diagnostic,
    private appVersion: AppVersion,
    public alertCtrl: AlertController,
    public geolocation: Geolocation
  ) {
    if (platform.is('ios') || platform.is('cordova') || platform.is('android') || platform.is('tablet') || platform.is('windows')) {
      apiProvider.isDevice = true;
    }

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.firebaseProvider.initializeFirebase();

      this.setupFirebase();

      this.setupBackgroundGeolocation();

      this.events.subscribe('LOGGED_IN', (status) => {
        if (status) {
          let url = this.apiProvider.getApiUrl() + this.apiProvider.apiMethods().updateUserCurrentLocation;

          const config: BackgroundGeolocationConfig = {
            desiredAccuracy: 10,
            stationaryRadius: 20,
            distanceFilter: 30,
            debug: false, //  enable this hear sounds for background-geolocation life-cycle.
            stopOnTerminate: false, // enable this to clear background location settings when the app terminates,
            url: url,
            httpHeaders: {
              'Content-Type': 'application/json'
            },
            postTemplate: {
              'user_id': this.storage.getUser().id,
              'latitude': '@latitude',
              'longitude': '@longitude'
            }
          };
          this.backgroundGeolocation.setConfig(config);
        } else {
          this.backgroundGeolocation.stop();
        }
      });
    });


    //Check app version number
    this.appVersion.getVersionNumber().then((version) => {
      const platformName = this.platform.is('android') ? 'android' : 'ios';

      this.apiProvider.appVersion(platformName, version).then((response: any) => {
        if (response.status) {
          const data = response.data;
          if (parseInt(data.updateRequired) == 1) {
            let alert = this.alertCtrl.create({
              title: data.alert_title,
              subTitle: data.alert_subtitle,
              enableBackdropDismiss: false,
              buttons: [{
                text: data.btn_title,
                role: 'ok',
                handler: () => {
                  const updateLink = data.redirect_link;
                  window.open(updateLink, '_system', 'location=yes');
                  return false;
                }
              }]
            });
            alert.present();
          }
        }
      }).catch((error) => {

      });
    }).catch((error) => {

    });
  }

  setupFirebase = () => {

    if (!this.apiProvider.isDevice) {
      return;
    }
    if (this.platform.is('ios')) {
      this.firebase.hasPermission().then(({isEnabled}) => {
        if (!isEnabled) {
          this.firebase.grantPermission();
        }
      });
    }

    this.firebase.setBadgeNumber(0);

    this.firebase.getToken()
      .then(token => {
        console.log(`The token is ${token}`);
        this.updateFirebaseToken(token);
      }) // save the token server-side and use it to push notifications to this device
      .catch(error => {
        console.error('Error getting token', error)
      });

    this.firebase.onTokenRefresh()
      .subscribe((token: string) => {
        this.updateFirebaseToken(token);
        console.log(`Got a new token ${token}`)
      });

    try {
      this.firebase.onNotificationOpen().subscribe((notification) => {
        if (!notification.tap) {
          this.apiProvider.showAlert('Tienes un nuevo mensaje en el chat, da clic en OK para visualizarlo.', () => {
            if (notification.action == 'order_assigned') {
              this.events.publish("REFRESH_ORDER");
            }
          });
        } else {
          this.apiProvider.showAlert('Tienes un nuevo mensaje en el chat, da clic en OK para visualizarlo.', () => {
            if (notification.action == 'order_assigned') {
              this.events.publish("REFRESH_ORDER");
            }
          });
        }
      });
    } catch (e) {

    }
  }

  updateFirebaseToken(token) {
    if (token) {
      localStorage.setItem('firebase_token', token);
      if (this.storage.getUser()) {
        this.apiProvider.updateToken(this.storage.getUser().id, token).then((data) => {
        });
      }
    }
  }

  setupBackgroundGeolocation = () => {
    setInterval(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        if (this.storage.getUser() && resp.coords) {
          let lat = resp.coords.latitude;
          let lng = resp.coords.longitude;
          if (this.platform.is('ios')) {
            this.backgroundGeolocation.startTask().then((taskKey) => {
              this.updateUserLocation(lat, lng, () => {
                this.backgroundGeolocation.endTask(taskKey);
                this.backgroundGeolocation.finish(); // FOR IOS ONLY
              });
            });
          } else {
            this.updateUserLocation(lat, lng, () => {
            });
          }
        }

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    }, 20 * 1000);

    this.startBackgroundLcationUpdate();

    this.backgroundGeolocation.isLocationEnabled().then((res) => {
      if (res) {
        // this.requestLocationPermission();
      } else {
        this.diagnostic.switchToLocationSettings();
      }
    });
    this.diagnostic.registerLocationStateChangeHandler((status) => {
      if (status) {
        // this.requestLocationPermission();
        this.startBackgroundLcationUpdate();
      }
    });
  }

  requestLocationPermission = () => {
    this.diagnostic.isLocationAuthorized().then((res) => {
      if (res) {
        this.startBackgroundLcationUpdate();
      } else {
        this.diagnostic.requestLocationAuthorization('always').then((access) => {
          if (access) {
            this.startBackgroundLcationUpdate();
          }
        })
      }
    })
  }

  startBackgroundLcationUpdate = () => {
    let url = this.apiProvider.getApiUrl() + this.apiProvider.apiMethods().updateUserCurrentLocation;

    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 30,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates,
      url: url,
      httpHeaders: {
        'Content-Type': 'application/json'
      },
      postTemplate: {
        'user_id': this.storage.getUser().id,
        'latitude': '@latitude',
        'longitude': '@longitude'
      }
    };

    this.backgroundGeolocation.configure(config).then(() => {
      this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe((location: BackgroundGeolocationResponse) => {
        console.log("Location " + JSON.stringify(location));

        if (this.storage.getUser() && location) {
          if (this.platform.is('ios')) {
            this.backgroundGeolocation.startTask().then((taskKey) => {
              this.updateUserLocation(location.latitude, location.longitude, () => {
                this.backgroundGeolocation.endTask(taskKey);
                this.backgroundGeolocation.finish(); // FOR IOS ONLY
              });
            });
          } else {
            this.updateUserLocation(location.latitude, location.longitude, () => {

            });
          }
        }
      });
    });

    // start recording location
    this.backgroundGeolocation.start();
  }

  updateUserLocation = (latitude, longitude, callBack = null) => {
    this.apiProvider.deliveryBoyLocationUpdate(this.storage.getUser().id, latitude, longitude).then(data => {
      if (callBack) {
        callBack();
      }
    }, (error) => {
      if (callBack) {
        callBack();
      }
    });
  }
}

