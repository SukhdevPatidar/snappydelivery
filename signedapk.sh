rm -rf snappygo-release-signed.apk
ionic cordova build android --prod --release
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore keystore/snappy.jks ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk snappy
zipalign -v 4 ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk snappygo-release-signed.apk
